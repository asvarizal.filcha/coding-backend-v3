<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Shopping;

class ShoppingController extends Controller
{

      

    public function shopping(Request $request){
        $input  = $request->all();


        $shopping = new Shopping;
        $shopping->CreatedDate = $input['shopping']['createddate'];
        $shopping->Name = $input['shopping']['name'];
        if($shopping->save()){ 
            $data = [
                'createddate'=>$shopping->CreatedDate,
                'id'=>$shopping->id,
                'name'=>$shopping->Name
             ];
            return response()->json([
                'data'=>$data
             ],200);
        } else{
            return response()->json([
                'message'=>'Incorrect Parameter'
             ],400);
        }

         
    }

    public function shoppingById($id){
         $shopping = Shopping::where('id', $id)->first();
        if ($shopping) {
            return response()->json([
                'createddate'=>$shopping->CreatedDate,
                'id'=>$shopping->id,
                'name'=>$shopping->Name
            ], 200);
        }else{
            return response()->json([
                'message' => 'Data Tidak Ditemukan'
            ], 400);
        }

         
    }

    public function UpdateShopping(Request $request,$id){
        $input  = $request->all();

        $shoppingArray = [
           'CreatedDate' => $input['shopping']['createddate'],
           'Name' => $input['shopping']['name'],
        ];
 
        Shopping::where('id',$id)->update($shoppingArray);    
        return response()->json([
                'message' => 'Data Berhasi Diubah'
            ], 200);
    }

    public function DeleteShopping($id){
         Shopping::where('id',$id)->delete();
         return response()->json([
                'message' => 'Data Berhasi Dihapus'
            ], 200);
    }

     
}
