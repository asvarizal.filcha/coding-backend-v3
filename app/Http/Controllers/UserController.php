<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

     protected function jwt(User $user) {
        $payload = [
            'iss' => "bearer", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, 'nfjkdndvuidbhvnjd','HS256');
    } 

    public function signup(Request $request){
         $input  = $request->all();


        $user = new User;
        $user->username = $input['user']['username'];
        $user->email = $input['user']['email'];
        $user->password = Hash::make($input['user']['encrypted_password']);
        $user->phone = $input['user']['phone'];
        $user->address = $input['user']['address'];
        $user->city = $input['user']['city'];
        $user->country = $input['user']['country'];
        $user->name = $input['user']['name'];
        $user->postcode = $input['user']['postcode'];
        if($user->save()){

            return response()->json([
                'email'=>$input['user']['email'],
                'token'=>$this->jwt($user),
                'username'=>$input['user']['username']
             ],200);

        } else{
            return response()->json([
                'message'=>'Incorrect Parameter'
             ],400);
        }

         
    }

     public function signin(Request $request){
         $input  = $request->all();

        $user = User::where('email', $input['email'])->first();
        if (!$user) {
            return response()->json([
                'error' => 'Email Tidak Terdaftar.'
            ], 400);
        }
        // Verify the password and generate the token
        if (Hash::check($input['password'], $user->password)) {
            return response()->json([
                'email'=>$user->email,
                'token'=>$this->jwt($user),
                'username'=>$user->username
            ], 200);
        }
        // Bad Request response
        return response()->json([
            'error' => 'Email Atau Password Salah.'
        ], 400);

         
    }
}
