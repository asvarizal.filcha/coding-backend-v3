<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/api/users/signup', 'UserController@signup');
$router->post('/api/users/signin', 'UserController@signin');

$router->group(
    ['middleware' => 'jwt.auth'], 
    function() use ($router) {
        $router->get('/api/users', function() {
            $users = \App\Models\User::all();
            return response()->json($users);
        });

        $router->get('/api/shopping', function() {
            $shopping = \App\Models\Shopping::all();
            return response()->json($shopping);
        });
        $router->post('/api/shopping', 'ShoppingController@shopping');
        $router->get('/api/shopping/{id}', 'ShoppingController@shoppingById');
        $router->put('/api/shopping/{id}', 'ShoppingController@UpdateShopping');
        $router->delete('/api/shopping/{id}', 'ShoppingController@DeleteShopping');

    }
);
